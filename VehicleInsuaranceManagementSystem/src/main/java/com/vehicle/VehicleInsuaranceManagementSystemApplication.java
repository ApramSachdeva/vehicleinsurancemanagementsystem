package com.vehicle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleInsuaranceManagementSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehicleInsuaranceManagementSystemApplication.class, args);
	}

}
