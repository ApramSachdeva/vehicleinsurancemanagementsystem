package com.vehicle.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vehicle.dao.CustomerDao;
import com.vehicle.models.Customer;

@Service
public class CustomerService {
	@Autowired
	private CustomerDao dao;
	
	public boolean addCustomer(Customer customer) {
		return dao.addCustomer(customer);
		
	}

	public int findCustomer(String userId,String pass) {
		Customer cust=new Customer();
		cust=dao.findCustomer(userId);
		if(cust==null) return 0;
		else if(pass.equals(cust.getPassword())) {
			return 1;
		}
		else {
			return 2;
		}
	}
	
}
