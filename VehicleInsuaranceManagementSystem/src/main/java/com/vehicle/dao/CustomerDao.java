package com.vehicle.dao;

import com.vehicle.models.Customer;

public interface CustomerDao {

	public boolean addCustomer(Customer customer);
	public Customer findCustomer(String userId);
	
}
